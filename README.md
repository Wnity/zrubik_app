# zrubik_app

#### 介绍
zframework写的zrubik降阶法解魔方程序,可达1000+阶,步速为1000+步/s,视机器性能而定,我的机器要15小时

#
源码: https://gitee.com/Wnity/zrubik

#
视频地址: https://www.zhihu.com/zvideo/1438880237841518592
#

有android, win64, osx平台

<img src="https://gitee.com/Wnity/zrubik_app/raw/master/preview.jpg" height="300"/>